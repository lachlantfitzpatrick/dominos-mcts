//
// Created by lachlan on 27/8/19.
//

#ifndef MCTS_GAMESTATE_H
#define MCTS_GAMESTATE_H

#include <utility>
#include <vector>
#include <algorithm>
#include <memory>

#include "Domino.h"
#include "DominoTrainArm.h"

namespace dominomcts {

    enum Player {ONE, TWO};

    struct GameState;
    using ImmutableGameState = std::shared_ptr<GameState>;
    struct GameState {

        // Members.
        const std::vector<ImmutableDomino> player_one_hand_;
        const std::vector<ImmutableDomino> player_two_hand_;
        const std::vector<ImmutableDominoTrainArm> domino_train_arms_;
        const std::vector<ImmutableDomino> pile_;
        const Player player_to_move_;
        const ImmutableDomino move;
        // Constructor.
        explicit GameState(std::vector<ImmutableDomino> player_one_hand,
                           std::vector<ImmutableDomino> player_two_hand,
                           std::vector<ImmutableDominoTrainArm> domino_train_arms,
                           std::vector<ImmutableDomino> pile,
                           Player player_to_move) :
                player_one_hand_(std::move(player_one_hand)),
                player_two_hand_(std::move(player_two_hand)),
                domino_train_arms_(std::move(domino_train_arms)),
                pile_(std::move(pile)),
                player_to_move_(player_to_move) {};
        explicit GameState(std::vector<ImmutableDomino> player_one_hand,
                        std::vector<ImmutableDomino> player_two_hand,
                        std::vector<ImmutableDominoTrainArm> domino_train_arms,
                        std::vector<ImmutableDomino> pile,
                        Player player_to_move, ImmutableDomino move) :
                        player_one_hand_(std::move(player_one_hand)),
                        player_two_hand_(std::move(player_two_hand)),
                        domino_train_arms_(std::move(domino_train_arms)),
                        pile_(std::move(pile)),
                        player_to_move_(player_to_move), move(std::move(move)) {};
        explicit GameState(const ImmutableGameState& other) :
                        player_one_hand_(other->player_one_hand_),
                        player_two_hand_(other->player_two_hand_),
                        domino_train_arms_(other->domino_train_arms_),
                        pile_(other->pile_),
                        player_to_move_(other->player_to_move_) {};
        // Methods.
        void GetDominoTrainFringes(std::vector<int> &ret);
        void PrintAll() const;
        void PrintPlayerToMove() const;
        void PrintPlayerOneHand() const;
        void PrintPlayerTwoHand() const;
        void PrintDominoTrain() const;
        void PrintPile() const;
    };

    ImmutableGameState ImmutableGameStateFactory(
            const std::vector<ImmutableDomino>& player_one_hand,
            const std::vector<ImmutableDomino>& player_two_hand,
            const std::vector<ImmutableDominoTrainArm>& domino_train_arms,
            const std::vector<ImmutableDomino>& pile,
            const Player& player_to_move);
    ImmutableGameState ImmutableGameStateFactory(
            const std::vector<ImmutableDomino>& player_one_hand,
            const std::vector<ImmutableDomino>& player_two_hand,
            const std::vector<ImmutableDominoTrainArm>& domino_train_arms,
            const std::vector<ImmutableDomino>& pile,
            const Player& player_to_move, const ImmutableDomino& move);
    ImmutableGameState ImmutableGameStateFactory(
            const ImmutableGameState& other);

}
#endif //MCTS_GAMESTATE_H
