//
// Created by lachlan on 27/8/19.
//

#include "Domino.h"

namespace dominomcts {

    int Domino::GetValue() const {
        return sides.first + sides.second;
    }

    int Domino::GetDominoFringe() const {
        return sides.second;
    }

    int Domino::GetDominoConnection() const {
        return sides.first;
    }

    bool Domino::CanConnect(int fringe) const {
        return fringe == sides.first || fringe == sides.second ||
            fringe == 0 || sides.first == 0 || sides.second == 0;
    }

    ImmutableDomino Domino::Connect(int fringe) const {
        return fringe == sides.first || fringe == 0 || sides.first == 0 ?
            ImmutableDominoFactory(sides.first, sides.second) :
            ImmutableDominoFactory(sides.second, sides.first);
    }

    void Domino::Print() const {
        std::cout << "[" << sides.first << "|" << sides.second << "]";
    }

    ImmutableDomino ImmutableDominoFactory(int side1, int side2) {
        return std::make_shared<Domino>(side1, side2);
    }

    ImmutableDomino ImmutableDominoFactory(const ImmutableDomino& other) {
        return std::make_shared<Domino>(other);
    }

}
