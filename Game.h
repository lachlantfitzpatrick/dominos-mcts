//
// Created by lachlan on 16/9/19.
//

#ifndef MCTS_GAME_H
#define MCTS_GAME_H

#include <random>

#include "GameState.h"
#include "Mcts.h"
#include "MctsNode.h"

namespace dominomcts {

    class Game {

    public:
        static void FullInformationGame();
        static MctsNode* HandlePlayerMove(MctsNode& robot_move);
        static ImmutableGameState GenerateGame();

    private:
        static std::vector<ImmutableDomino> GenerateDominos();

    };

}


#endif //MCTS_GAME_H
