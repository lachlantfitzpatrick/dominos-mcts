//
// Created by lachlan on 27/8/19.
//

#ifndef MCTS_DOMINOTRAINARM_H
#define MCTS_DOMINOTRAINARM_H

#include <vector>
#include <utility>
#include <memory>

#include "Domino.h"

namespace dominomcts {

    struct DominoTrainArm;
    using ImmutableDominoTrainArm = std::shared_ptr<DominoTrainArm>;
    struct DominoTrainArm {

        // Members.
        const std::vector<ImmutableDomino> arm;
        // Constructors.
        explicit DominoTrainArm(std::vector<ImmutableDomino> arm) :
            arm(std::move(arm)) { };
        explicit DominoTrainArm(const ImmutableDominoTrainArm& other) :
            arm(other->arm) { };
        // Methods.
        int GetDominoTrainFringe() const;
        std::vector<ImmutableDomino> GetArm() const;
        ImmutableDominoTrainArm ExtendArm(const ImmutableDomino& extension) const;
        void Print() const;
    };

    ImmutableDominoTrainArm ImmutableDominoTrainArmFactory(
            const std::vector<ImmutableDomino>& arm);
    ImmutableDominoTrainArm ImmutableDominoTrainArmFactory(
            const ImmutableDominoTrainArm& other);
    ImmutableDominoTrainArm ImmutableDominoTrainArmFactory(
            const ImmutableDominoTrainArm& other,
            const ImmutableDomino& extension);

}

#endif //MCTS_DOMINOTRAINARM_H
