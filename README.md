# About

This project was built to test the hypothesis that an MCTS algorithm would
win the modified game of dominos used in the domino playing robot project
more often than the original greedy-best algorithm.

The result of this project is that the computer wins 90% of the time against
a human and 100% of the time when playing against the greedy-best algorithm
that was originally implemented. These were both tested over 20 games against
a human.

# How to Play Dominos

In this version of dominos the goal is to empty your hand first or have the
smallest domino count when neither player can make a move.

The computer will play first and then the game proceeds on a turn basis such
that each player can play at most one domino in their turn. A domino can be
played if one of its sides matches the a side on the edge of a domino
train arm (these are the displayed in the CLI every turn).

Good luck! The computer is hard to beat ;)

# Build

`cmake` was used to build and operate this project. To build and run the
project run `./mcts` in the root directory.

It has been tested on an Ubuntu 18.04 with cmake 3.10.
