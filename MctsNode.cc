//
// Created by lachlan on 27/8/19.
//

#include "MctsNode.h"

namespace dominomcts {

    MctsNode::MctsNode(ImmutableGameState& state) :
            state_(state), simulations_(0), player_wins_(0), parent_(nullptr),
            is_root_(true) { }

    MctsNode::MctsNode(ImmutableGameState& state, MctsNode* parent) :
            state_(state), parent_(parent), simulations_(0),
            player_wins_(0), is_root_(false) {
        parent->AddChild(this);
    }

    MctsNode* MctsNode::MoveFactory(
            MctsNode *parent, const ImmutableDomino& move, int arm) {
        // Wild card case.
        int arm_fringe = parent->GetGameState()->domino_train_arms_[arm]->GetDominoTrainFringe();
        if (arm_fringe == 0 || (move->sides.first == 0 && move->sides.second == arm_fringe) ||
            (move->sides.second == 0 && move->sides.first == arm_fringe)) {
            ImmutableDomino other_move =
                    ImmutableDominoFactory(move->sides.second, move->sides.first);
            std::vector<ImmutableDomino> new_hand =
                    parent->GetGameState()->player_to_move_ == ONE ?
                    parent->GetGameState()->player_one_hand_ :
                    parent->GetGameState()->player_two_hand_;
            new_hand.erase(std::find(new_hand.begin(),
                                     new_hand.end(), move));
            std::vector<ImmutableDominoTrainArm> new_arms =
                    parent->GetGameState()->domino_train_arms_;
            auto new_arm = new_arms[arm];
            new_arms.erase(new_arms.begin()+arm);
            new_arms.insert(new_arms.begin()+arm,
                            ImmutableDominoTrainArmFactory(new_arm, other_move));
            ImmutableGameState new_game_state =
                    parent->GetGameState()->player_to_move_ == ONE ?
                    ImmutableGameStateFactory(
                            new_hand,
                            parent->GetGameState()->player_two_hand_,
                            new_arms,
                            parent->GetGameState()->pile_,
                            TWO, move) :
                    ImmutableGameStateFactory(
                            parent->GetGameState()->player_one_hand_,
                            new_hand,
                            new_arms,
                            parent->GetGameState()->pile_,
                            ONE, move);
            new MctsNode(new_game_state, parent);
        }
        // Default case.
        std::vector<ImmutableDomino> new_hand =
                parent->GetGameState()->player_to_move_ == ONE ?
                parent->GetGameState()->player_one_hand_ :
                parent->GetGameState()->player_two_hand_;
        new_hand.erase(std::find(new_hand.begin(),
                                 new_hand.end(), move));
        std::vector<ImmutableDominoTrainArm> new_arms =
                parent->GetGameState()->domino_train_arms_;
        auto new_arm = new_arms[arm];
        new_arms.erase(new_arms.begin()+arm);
        new_arms.insert(new_arms.begin()+arm,
                        ImmutableDominoTrainArmFactory(new_arm, move));
        ImmutableGameState new_game_state =
                parent->GetGameState()->player_to_move_ == ONE ?
                ImmutableGameStateFactory(
                        new_hand,
                        parent->GetGameState()->player_two_hand_,
                        new_arms,
                        parent->GetGameState()->pile_,
                        TWO, move) :
                ImmutableGameStateFactory(
                        parent->GetGameState()->player_one_hand_,
                        new_hand,
                        new_arms,
                        parent->GetGameState()->pile_,
                        ONE, move);
        return new MctsNode(new_game_state, parent);
    }

    MctsNode* MctsNode::PassFactory(MctsNode *parent) {
        ImmutableGameState new_game_state =
                parent->GetGameState()->player_to_move_ == ONE ?
                ImmutableGameStateFactory(
                        parent->GetGameState()->player_one_hand_,
                        parent->GetGameState()->player_two_hand_,
                        parent->GetGameState()->domino_train_arms_,
                        parent->GetGameState()->pile_,
                        TWO
                        ) :
                ImmutableGameStateFactory(
                        parent->GetGameState()->player_one_hand_,
                        parent->GetGameState()->player_two_hand_,
                        parent->GetGameState()->domino_train_arms_,
                        parent->GetGameState()->pile_,
                        ONE
                );
        return new MctsNode(new_game_state, parent);
    }

    MctsNode* MctsNode::DrawFactory(MctsNode *parent, int pile_index) {
        std::vector<ImmutableDomino> new_hand =
                parent->GetGameState()->player_to_move_ == ONE ?
                        parent->GetGameState()->player_one_hand_ :
                        parent->GetGameState()->player_two_hand_;
        new_hand.push_back(parent->GetGameState()->pile_[pile_index]);
        std::vector<ImmutableDomino> new_pile = parent->GetGameState()->pile_;
        new_pile.erase(new_pile.begin() + pile_index);
        ImmutableGameState new_game_state =
                parent->GetGameState()->player_to_move_ == ONE ?
                ImmutableGameStateFactory(
                        new_hand,
                        parent->GetGameState()->player_two_hand_,
                        parent->GetGameState()->domino_train_arms_,
                        new_pile,
                        ONE,
                        parent->GetGameState()->pile_[pile_index]
                        ) :
                ImmutableGameStateFactory(
                        parent->GetGameState()->player_one_hand_,
                        new_hand,
                        parent->GetGameState()->domino_train_arms_,
                        new_pile,
                        TWO,
                        parent->GetGameState()->pile_[pile_index]
                        );
        return new MctsNode(new_game_state, parent);
    }

    void MctsNode::SetChildren(std::vector<MctsNode>& children) {
        for(auto & child : children) {
            AddChild(&child);
        }
    }

    void MctsNode::Update(Player simulation_winner) {
        if (simulation_winner == state_->player_to_move_) {
            player_wins_++;
        }
        simulations_++;
    }

    void MctsNode::UpdateWithPlayerOneWins(int player_one_wins,
            int simulations) {
        simulations_ += simulations;
        if (state_->player_to_move_ == ONE) {
            player_wins_ += player_one_wins;
        } else {
            player_wins_ += (simulations - player_one_wins);
        }
    }

    MctsNode* MctsNode::SelectChild() {
        MctsNode* priorityChild = children_[0];
        double best_value = children_[0]->GetUctValue();
        double potential_best_value;
        for (auto & child : children_) {
            potential_best_value = child->GetUctValue();
            if (potential_best_value > best_value) {
                priorityChild = child;
                best_value = potential_best_value;
            }
        }
        return priorityChild;
    }

    void MctsNode::AddChild(MctsNode* child) {
        children_.push_back(child);
        child->parent_ = this;
    }

    void MctsNode::DeleteChildren() {
        for (auto child : GetChildren()) {
            delete child;
        }
        children_.clear();
    }

    bool MctsNode::IsMctsFringe() {
        return children_.empty();
    }

    bool MctsNode::IsMctsRoot() {
        return is_root_;
    }

    double MctsNode::GetUctValue() {
        return simulations_ > 0 ? (state_->player_to_move_ == parent_->state_->player_to_move_ ?
                ((double)player_wins_) / simulations_ +
               4*sqrt(log(parent_->simulations_) / simulations_) :
               ((double)(simulations_ - player_wins_)) / simulations_ +
               4*sqrt(log(parent_->simulations_) / simulations_)) : -INFINITY;
    }

    ImmutableGameState MctsNode::GetGameState() {
        return state_;
    }

    MctsNode* MctsNode::GetParent() {
        return parent_;
    }

    int MctsNode::GetSimulations() {
        return simulations_;
    }

    int MctsNode::GetPlayerWins() {
        return player_wins_;
    }

    int MctsNode::GetNumberChildren() {
        return children_.size();
    }

    std::vector<MctsNode*> MctsNode::GetChildren() {
        return children_;
    }

    double MctsNode::GetPlayerOneValue() {
        return state_->player_to_move_ == ONE ?
               ((double)player_wins_)/simulations_ :
               ((double)(simulations_ - player_wins_))/simulations_;
    }

    double MctsNode::GetPlayerTwoValue() {
        return state_->player_to_move_ == TWO ?
               ((double)player_wins_)/simulations_ :
               ((double)(simulations_ - player_wins_))/simulations_;
    }

}