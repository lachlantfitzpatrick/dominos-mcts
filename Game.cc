//
// Created by lachlan on 16/9/19.
//

#include "Game.h"

namespace dominomcts {


    void Game::FullInformationGame() {
        std::cout << "Player ONE is robot and Player TWO is human." <<
            std::endl;
        auto state = GenerateGame();
        state->PrintAll();
        auto* root = new MctsNode(state);
        while (!Mcts::IsGameOver(root)) {
            if (root->GetGameState()->player_to_move_ == ONE) {
                std::cout << "Robot is moving..." << std::endl;
                auto robot_move =
                        Mcts::GetBestMoveWithFullInformationSearch(*root, 100000, 15000);
                std::cout << "Move complete... probability of robot win: " <<
                    robot_move->GetPlayerOneValue() << std::endl;
                robot_move->GetGameState()->PrintAll();
                auto new_game_state = robot_move->GetGameState();
                auto* robot_move_copy = new MctsNode(new_game_state);
                root->DeleteChildren();
                root->AddChild(robot_move_copy);
                root = robot_move_copy;
            } else if (root->GetGameState()->player_to_move_ == TWO) {
                std::cout << "It's your move human." << std::endl;
                auto player_move = HandlePlayerMove(*root);
                player_move->GetGameState()->PrintAll();
                auto new_game_state = player_move->GetGameState();
                auto* player_move_copy = new MctsNode(new_game_state);
                root->DeleteChildren();
                root->AddChild(player_move_copy);
                root = player_move_copy;
            }
        }
        Player winner = Mcts::DetermineWinner(root);
        if (winner == ONE) {
            std::cout << "Player ONE wins. (Robots are superior.)" << std::endl;
        } else if (winner == TWO) {
            std::cout << "Player TWO wins." << std::endl;
        }
    }

    MctsNode* Game::HandlePlayerMove(MctsNode &robot_move) {
        while (true) {
            std::string response;
            while (true) {
                std::cout << "[P]lay a domino, [D]raw a domino, P[a]ss your turn"
                          << std::endl;
                std::cin >> response;
                if (response == "p" || response == "P") {
                    response = "p";
                    break;
                }
                if (response == "d" || response == "D") {
                    if (robot_move.GetGameState()->pile_.empty()) {
                        std::cout << "Sorry, there are no dominos to draw "
                                     "from the pile. Please choose a "
                                     "different action to take." << std::endl;
                    } else {
                        response = "d";
                        break;
                    }
                }
                if (response == "a" || response == "A") {
                    if (!robot_move.GetGameState()->pile_.empty()) {
                        std::cout << "Sorry, there are dominos on the pile "
                                     "so you must draw from the pile until it"
                                     " is empty before passing your turn. "
                                     "Please choose a different action to "
                                     "take." << std::endl;
                    } else {
                        response = "a";
                        break;
                    }
                }
                std::cout << "Sorry that wasn't a valid command. Enter a "
                             "letter to indicate the action to take." <<
                             std::endl;
            }
            if (response == "a") {
                return MctsNode::PassFactory(&robot_move);
            }
            else if (response == "d") {
                while (true) {
                    std::cout << "Which domino would you like to pickup?" <<
                              std::endl;
                    std::cout << "(Enter two numbers consecutively representing "
                                 "the sides of the domino you'd like to pickup.)"
                              << std::endl;
                    std::cin >> response;
                    if (response.size() == 2) {
                        try {
                            int first_side = std::stoi(response.substr(0,1));
                            int second_side = std::stoi(response.substr(1,1));
                            for (int i = 0;
                                 i < robot_move.GetGameState()->pile_.size();
                                 i++) {
                                auto domino =
                                        robot_move.GetGameState()->pile_[i];
                                if ((domino->sides.first == first_side &&
                                     domino->sides.second == second_side) ||
                                    (domino->sides.first == second_side &&
                                     domino->sides.second == first_side)) {
                                    return MctsNode::DrawFactory(&robot_move, i);
                                }
                            }
                            std::cout << "Sorry, there were no dominos in the pile"
                                         " that matched your selection. Please "
                                         "select another action." << std::endl;
                            break;
                        }
                        catch (int e) {
                            std::cout << "Sorry, you must enter 2 "
                                         "consecutive numbers representing the "
                                         "sides of a domino. Please select "
                                         "another action." << std::endl;
                            break;
                        }
                    } else {
                        std::cout << "Sorry, you must enter 2 "
                                     "consecutive numbers representing the "
                                     "sides of a domino. Please select "
                                     "another action." << std::endl;
                        break;
                    }
                }
            } else if(response == "p") {
                std::cout << "Which domino would you like to play?" <<
                          std::endl;
                std::cout << "(Enter two numbers consecutively representing "
                             "the sides of the domino you'd like to play with the side to connect"
                             " to the domino train first.)"
                          << std::endl;
                std::cin >> response;
                if (response.size() == 2) {
                    try {
                        int first_side = std::stoi(response.substr(0,1));
                        int second_side = std::stoi(response.substr(1,1));
                        bool error = false;
                        for (int i = 0;
                             i < robot_move.GetGameState()->player_two_hand_.size(); i++) {
                            auto domino = robot_move.GetGameState()->player_two_hand_[i];
                            if ((domino->sides.first == first_side &&
                                 domino->sides.second == second_side) ||
                                (domino->sides.first == second_side &&
                                 domino->sides.second == first_side)) {
                                std::cout << "Which arm of the domino train "
                                             "would you like to connect it "
                                             "to?" << std::endl;
                                std::cout << "(Enter 1,2,3 or 4.)" << std::endl;
                                std::cin >> response;
                                if (response.size() == 1) {
                                    try {
                                        int arm = std::stoi(response)-1;
                                        if (!(arm >= 0 && arm <= 3)) {
                                            std::cout << "Sorry, that wasn't a number between 1 and 4"
                                                         ". Please select another action." << std::endl;
                                            error = true;
                                            break;
                                        }
                                        else if (robot_move.GetGameState()->domino_train_arms_[arm]
                                            ->GetDominoTrainFringe() == first_side ||
                                            first_side == 0 ||
                                            robot_move.GetGameState()->domino_train_arms_[arm]
                                            ->GetDominoTrainFringe() == 0) {
                                            return MctsNode::MoveFactory(&robot_move, domino,
                                                    arm);
                                        } else {
                                            std::cout << "Sorry, the domino couldn't be connected"
                                                         " to the domino train. Please select "
                                                         "another action." << std::endl;
                                            error = true;
                                            break;
                                        }
                                    } catch (int e) {
                                        std::cout << "Sorry, that wasn't a number between 1 and 4"
                                                     ". Please select another action." << std::endl;
                                        error = true;
                                        break;
                                    }
                                }
                            }
                        }
                        if (!error) {
                            std::cout << "Sorry, there were no dominos in your hand"
                                         " that matched your selection. Please "
                                         "select another action." << std::endl;
                        }
                    }
                    catch (int e) {
                        std::cout << "Sorry, you must enter 2 "
                                     "consecutive numbers representing the "
                                     "sides of a domino." << std::endl;
                    }
                } else {
                    std::cout << "Sorry, you must enter 2 "
                                 "consecutive numbers representing the "
                                 "sides of a domino." << std::endl;
                }
            }
        }
    }

    ImmutableGameState Game::GenerateGame() {
        std::random_device rd;
        std::mt19937 rng(rd());
        auto dominos = GenerateDominos();
        dominomcts::ImmutableDomino spinner = dominomcts::ImmutableDominoFactory(-1,-1);
        std::vector<dominomcts::ImmutableDomino> player_one_hand;
        player_one_hand.reserve(7);
        std::vector<dominomcts::ImmutableDomino> player_two_hand;
        player_one_hand.reserve(7);
        std::vector<dominomcts::ImmutableDomino> pile;
        player_one_hand.reserve(5);
        std::uniform_int_distribution<int> uni_dist(0, dominos.size()-1);
        while (spinner->sides.first == -1) {
            auto random_index = uni_dist(rng);
            if (dominos[random_index]->sides.first ==
                dominos[random_index]->sides.second) {
                spinner = dominos[random_index];
                dominos.erase(dominos.begin()+random_index);
            }
        }
        for (int i = 0; i < 7; i++) {
            uni_dist = std::uniform_int_distribution<int>(0, dominos.size()-1);
            auto random_index = uni_dist(rng);
            player_one_hand.push_back(dominos[random_index]);
            dominos.erase(dominos.begin()+random_index);
        }
        for (int i = 0; i < 7; i++) {
            uni_dist = std::uniform_int_distribution<int>(0, dominos.size()-1);
            auto random_index = uni_dist(rng);
            player_two_hand.push_back(dominos[random_index]);
            dominos.erase(dominos.begin()+random_index);
        }
        for (int i = 0; i < 5; i++) {
            uni_dist = std::uniform_int_distribution<int>(0, dominos.size()-1);
            auto random_index = uni_dist(rng);
            pile.push_back(dominos[random_index]);
            dominos.erase(dominos.begin()+random_index);
        }
        return dominomcts::ImmutableGameStateFactory(
                player_one_hand,
                player_two_hand,
                {
                        dominomcts::ImmutableDominoTrainArmFactory({spinner}),
                        dominomcts::ImmutableDominoTrainArmFactory({spinner}),
                        dominomcts::ImmutableDominoTrainArmFactory({spinner}),
                        dominomcts::ImmutableDominoTrainArmFactory({spinner})
                },
                pile,
                dominomcts::ONE
        );
    }

    std::vector<ImmutableDomino> Game::GenerateDominos() {
        std::vector<dominomcts::ImmutableDomino> dominos;
        dominos.reserve(28);
        for (int i = 0; i <= 6; i++) {
            for (int j = i; j <=6; j++) {
                dominos.push_back(dominomcts::ImmutableDominoFactory(i,j));
            }
        }
        return dominos;
    }
}