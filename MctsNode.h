//
// Created by lachlan on 27/8/19.
//

#ifndef MCTS_MCTSNODE_H
#define MCTS_MCTSNODE_H

#include <cmath>

#include "GameState.h"

namespace dominomcts {

    class MctsNode {

        // Members.
        ImmutableGameState state_;
        bool is_root_;
        int player_wins_;
        int simulations_;
        std::vector<MctsNode*> children_;
        MctsNode* parent_;

    public:
        // Constructor.
        MctsNode(ImmutableGameState& state, MctsNode* parent);
        explicit MctsNode(ImmutableGameState& state);
        static MctsNode* MoveFactory(MctsNode* parent,
                const ImmutableDomino& move, int arm);
        static MctsNode* PassFactory(MctsNode* parent);
        static MctsNode* DrawFactory(MctsNode* parent, int pile_index);
        // Methods.
        void SetChildren(std::vector<MctsNode>& children);
        ImmutableGameState GetGameState();
        MctsNode* GetParent();
        void Update(Player simulation_winner);
        void UpdateWithPlayerOneWins(int player_one_wins, int simulations);
        MctsNode* SelectChild();
        void AddChild(MctsNode* child);
        void DeleteChildren();
        bool IsMctsFringe();
        bool IsMctsRoot();
        double GetUctValue();
        int GetSimulations();
        int GetPlayerWins();
        double GetPlayerOneValue();
        double GetPlayerTwoValue();
        int GetNumberChildren();
        std::vector<MctsNode*> GetChildren();

    };

}


#endif //MCTS_MCTSNODE_H
