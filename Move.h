//
// Created by lachlan on 27/8/19.
//

#ifndef MCTS_MOVE_H
#define MCTS_MOVE_H

#include <utility>
#include <memory>

#include "Domino.h"

namespace dominomcts {

    enum MoveType {DOMINO, DRAW, PASS};

    struct Move;
    using ImmutableMove = std::shared_ptr<Move>;
    struct Move {

        // Members.
        const MoveType move_type_;
        const ImmutableDomino domino_played_;
        const int fringe_played_;
        // Constructors.
        explicit Move(MoveType move_type,
                ImmutableDomino domino_played, int fringe_played) :
                move_type_(move_type),
                domino_played_(std::move(domino_played)),
                fringe_played_(fringe_played) {};
        explicit Move(const ImmutableMove& other) :
                move_type_(other->move_type_),
                domino_played_(other->domino_played_),
                fringe_played_(other->fringe_played_) { };

    };

    ImmutableMove ImmutableMoveFactory(MoveType move_type,
            const ImmutableDomino& domino_played = ImmutableDominoFactory(-1, -1),
            int fringe_played = -1) {
        return std::make_shared<Move>(move_type, domino_played, fringe_played);
    }
    ImmutableMove ImmutableMoveFactory(const ImmutableMove& other) {
        return std::make_shared<Move>(other);
    }

}

#endif //MCTS_MOVE_H
