#include <iostream>
#include <random>

#include "GameState.h"
#include "MctsNode.h"
#include "Domino.h"
#include "Mcts.h"
#include "Game.h"

int main() {
    dominomcts::Game::FullInformationGame();
}

// Test of the MCTS full information algorithm.
//int main() {
//    auto initial_state = dominomcts::Game::GenerateGame();
//    dominomcts::MctsNode root(initial_state);
//    dominomcts::Mcts::FullInformationSearch(root, 100000);
//    std::random_device rd;
//    std::mt19937 rng(rd());
//    dominomcts::MctsNode* current = &root;
//    do {
//        current->GetGameState()->PrintAll();
//        std::uniform_int_distribution<int> uni_dist(0,
//                current->GetNumberChildren()-1);
//        current = current->GetChildren()[uni_dist(rng)];
//    } while (!current->IsMctsFringe());
//}

// Test of the datastructures.
//int main() {
//    auto d1 = dominomcts::ImmutableDominoFactory(1,2);
//    auto d2 = dominomcts::ImmutableDominoFactory(2,2);
//    auto d3 = dominomcts::ImmutableDominoFactory(3,2);
//    std::cout << d1->sides.first << std::endl;
//    std::cout << d2->sides.first << std::endl;
//    std::cout << d3->sides.first << std::endl;
//
//    auto dta1 = dominomcts::ImmutableDominoTrainArmFactory({d1, d2, d3});
//    auto dta2 = dominomcts::ImmutableDominoTrainArmFactory({d1, d2, d3});
//    std::cout << dta1->arm[0] << std::endl;
//    std::cout << d1 << std::endl;
//
//    auto gs = dominomcts::ImmutableGameStateFactory({d1}, {d2}, {dta1}, {d3},
//            dominomcts::ONE);
//    std::cout << gs->domino_train_arms_[0]->arm[0] << std::endl;
//    std::cout << gs->player_one_hand_[0] << std::endl;
//
//    dominomcts::MctsNode root(gs);
//    dominomcts::MctsNode n1(gs, &root);
//    dominomcts::MctsNode n2(gs, &root);
//    dominomcts::MctsNode n3(gs, &root);
//    std::cout << &root << std::endl;
//    std::cout << n1.GetParent() << std::endl;
//    n1.Update(dominomcts::ONE);
//    n1.Update(dominomcts::ONE);
//    n2.Update(dominomcts::ONE);
//    n2.Update(dominomcts::TWO);
//    n3.Update(dominomcts::TWO);
//    n3.Update(dominomcts::TWO);
//    root.Update(dominomcts::ONE);
//    root.Update(dominomcts::ONE);
//    root.Update(dominomcts::ONE);
//    root.Update(dominomcts::TWO);
//    root.Update(dominomcts::TWO);
//    root.Update(dominomcts::TWO);
//    std::cout << &n1 << " " << n1.GetUctValue() << std::endl;
//    std::cout << &n2 << " " << n2.GetUctValue() << std::endl;
//    std::cout << &n3 << " " << n3.GetUctValue() << std::endl;
//    auto best = root.SelectChild();
//    std::cout << best << " " << best->GetUctValue() << std::endl;
//    auto children = root.GetChildren();
//    std::cout << children[0]->GetSimulations() << std::endl;
//    std::cout << children[1]->GetUctValue() << std::endl;
//    std::cout << children[2]->GetSimulations() << std::endl;
//
//    return 0;
//}