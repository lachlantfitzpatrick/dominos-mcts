//
// Created by lachlan on 27/8/19.
//

#ifndef MCTS_DOMINO_H
#define MCTS_DOMINO_H

#include <utility>
#include <memory>
#include <iostream>

namespace dominomcts {

    /**
     * A domino represented by two integers. If placed on a domino train then the
     * first value is taken as the connection and the second as the fringe.
     */
    struct Domino;
    using ImmutableDomino = std::shared_ptr<Domino>;
    struct Domino {

        // Members.
        const std::pair<int, int> sides;
        // Constructors.
        Domino(int side1, int side2) : sides{side1, side2} { };
        explicit Domino(const ImmutableDomino& other) :
            sides{other->sides.first, other->sides.second} {};
        // Methods.
        int GetValue() const;
        int GetDominoConnection() const;
        int GetDominoFringe() const;
        bool CanConnect(int fringe) const;
        ImmutableDomino Connect(int fringe) const;
        void Print() const;

    };
    ImmutableDomino ImmutableDominoFactory(int side1, int side2);
    ImmutableDomino ImmutableDominoFactory(const ImmutableDomino& other);

}

#endif //MCTS_DOMINO_H
