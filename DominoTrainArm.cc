//
// Created by lachlan on 27/8/19.
//

#include "DominoTrainArm.h"

namespace dominomcts {

    int DominoTrainArm::GetDominoTrainFringe() const {
        return arm.back()->GetDominoFringe();
    }

    std::vector<ImmutableDomino> DominoTrainArm::GetArm() const {
        return arm;
    }

    ImmutableDominoTrainArm DominoTrainArm::ExtendArm(
            const ImmutableDomino& extension) const {
        std::vector<ImmutableDomino> newArm(arm);
        newArm.push_back(extension);
        return ImmutableDominoTrainArmFactory(newArm);
    }

    void DominoTrainArm::Print() const {
        for(const auto& domino : arm) {
            domino->Print();
        }
    }

    ImmutableDominoTrainArm ImmutableDominoTrainArmFactory(
            const std::vector<ImmutableDomino>& arm) {
        return std::make_shared<DominoTrainArm>(arm);
    }
    ImmutableDominoTrainArm ImmutableDominoTrainArmFactory(
            const ImmutableDominoTrainArm& other) {
        return std::make_shared<DominoTrainArm>(other);
    }
    ImmutableDominoTrainArm ImmutableDominoTrainArmFactory(
            const ImmutableDominoTrainArm& other,
            const ImmutableDomino& extension) {
        std::vector<ImmutableDomino> new_arm = other->arm;
        ImmutableDomino connected_extension = extension->Connect(
                other->GetDominoTrainFringe());
        new_arm.push_back(connected_extension);
        return ImmutableDominoTrainArmFactory(new_arm);
    }

}
