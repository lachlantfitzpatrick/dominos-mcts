//
// Created by lachlan on 27/8/19.
//

#include "GameState.h"

#include <utility>

namespace dominomcts {

    void GameState::GetDominoTrainFringes(std::vector<int>& ret) {
        std::transform(domino_train_arms_.begin(), domino_train_arms_.end(),
                       std::back_inserter(ret),
                       [](auto a) -> int { return a->GetDominoTrainFringe(); });
    }

    void GameState::PrintAll() const {
        PrintPlayerToMove();
        PrintPlayerOneHand();
        PrintPlayerTwoHand();
        PrintDominoTrain();
        PrintPile();
    }

    void GameState::PrintPlayerToMove() const {
        std::cout << "Player " << (player_to_move_ == ONE ? "ONE" : "TWO")
                  << " is to move" << std::endl;
    }

    void GameState::PrintPlayerOneHand() const {
        std::cout << "Player one hand: " << std::endl;
        for (const auto& domino : player_one_hand_) {
            domino->Print();
        }
        std::cout << std::endl;
    }

    void GameState::PrintPlayerTwoHand() const {
        std::cout << "Player two hand: " << std::endl;
        for (const auto& domino : player_two_hand_) {
            domino->Print();
        }
        std::cout << std::endl;
    }

    void GameState::PrintDominoTrain() const {
        std::cout << "Domino train: " << std::endl;
        int counter = 1;
        for (const auto& arm : domino_train_arms_) {
            std::cout << counter << ": ";
            counter++;
            arm->Print();
            std::cout << std::endl;
        }
    }

    void GameState::PrintPile() const {
        std::cout << "Pile: " << std::endl;
        for (const auto& domino : pile_) {
            domino->Print();
        }
        std::cout << std::endl;
    }

    ImmutableGameState ImmutableGameStateFactory(
            const std::vector<ImmutableDomino>& player_one_hand,
            const std::vector<ImmutableDomino>& player_two_hand,
            const std::vector<ImmutableDominoTrainArm>& domino_train_arms,
            const std::vector<ImmutableDomino>& pile,
            const Player& player_to_move) {
        return std::make_shared<GameState>(
                player_one_hand, player_two_hand, domino_train_arms,
                pile, player_to_move);
    }
    ImmutableGameState ImmutableGameStateFactory(
            const std::vector<ImmutableDomino>& player_one_hand,
            const std::vector<ImmutableDomino>& player_two_hand,
            const std::vector<ImmutableDominoTrainArm>& domino_train_arms,
            const std::vector<ImmutableDomino>& pile,
            const Player& player_to_move, const ImmutableDomino& move) {
        return std::make_shared<GameState>(
                player_one_hand, player_two_hand, domino_train_arms,
                pile, player_to_move, move);
    }
    ImmutableGameState ImmutableGameStateFactory(
            const ImmutableGameState& other) {
        return std::make_shared<GameState>(other);
    }

}