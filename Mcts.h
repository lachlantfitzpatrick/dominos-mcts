//
// Created by lachlan on 27/8/19.
//

#ifndef MCTS_MCTS_H
#define MCTS_MCTS_H

#include <random>
#include <algorithm>
#include <thread>

#include "MctsNode.h"

namespace dominomcts {

    class Mcts{

    public:
        static void FullInformationSearch(MctsNode& root, int simulations, long milliseconds);
        static bool IsGameOver(MctsNode* node);
        static dominomcts::Player DetermineWinner(MctsNode* node);
        static MctsNode* GetBestMoveWithFullInformationSearch(
                MctsNode& root, int simulations, long milliseconds);
    private:
        static MctsNode* SelectFringe(MctsNode* root);
        static MctsNode* SelectChild(MctsNode* node);
        static void ExpandFringe(MctsNode* tree_fringe);
        static void SimulateFringes(MctsNode* tree_fringe_parent);
        static void SimulateFringe(MctsNode* tree_fringe);
        static void BackpropagateExpandedFringe(MctsNode* tree_fringe_parent);
        static void BackpropagateFinalFringe(MctsNode* final_fringe, Player winner);

    };

}


#endif //MCTS_MCTS_H
