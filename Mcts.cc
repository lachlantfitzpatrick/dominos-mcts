//
// Created by lachlan on 27/8/19.
//

#include "Mcts.h"

namespace dominomcts {

    void Mcts::FullInformationSearch(MctsNode &root, int simulations, long milliseconds) {
        auto timer = std::chrono::system_clock::now().time_since_epoch().count();
        for (int i = 0; i < simulations; i++) {
            auto best = SelectFringe(&root);
            if (IsGameOver(best)) {
                auto winner = DetermineWinner(best);
                best->Update(winner); // This behaves as simulation.
                BackpropagateFinalFringe(best, winner);
            } else {
                ExpandFringe(best);
                SimulateFringes(best);
                BackpropagateExpandedFringe(best);
            }
            if (std::chrono::system_clock::now().time_since_epoch().count() - timer >
                milliseconds*1000000) break;
        }
    }

    MctsNode* Mcts::GetBestMoveWithFullInformationSearch(
            MctsNode &root, int simulations, long milliseconds) {
        FullInformationSearch(root, simulations, milliseconds);
        auto best = root.GetChildren()[0];
        double best_probability = best->GetPlayerOneValue();
        for (auto child : root.GetChildren()) {
            if (best_probability < child->GetPlayerOneValue()) {
                best = child;
                best_probability = child->GetPlayerOneValue();
            }
        }
        return best;
    }

    MctsNode* Mcts::SelectFringe(MctsNode *root) {
        MctsNode* best = root;
        while (!best->IsMctsFringe()) {
            best = SelectChild(best);
        }
        return best;
    }

    MctsNode* Mcts::SelectChild(MctsNode *node) {
        auto children = node->GetChildren();
        MctsNode* priorityChild = children[0];
        double best_value = children[0]->GetUctValue();
        double potential_best_value;
        for (auto child : children) {
            potential_best_value = child->GetUctValue();
            if (potential_best_value > best_value) {
                priorityChild = child;
                best_value = potential_best_value;
            }
        }
        return priorityChild;
    }

    void Mcts::ExpandFringe(MctsNode *tree_fringe) {
        std::vector<int> fringes;
        fringes.reserve(4);
        for (int i = 0;
            i < tree_fringe->GetGameState()->domino_train_arms_.size(); i++) {
            if (std::find(fringes.begin(), fringes.end(),
                          tree_fringe->GetGameState()->
                          domino_train_arms_[i]->GetDominoTrainFringe()) !=
                          fringes.end()) {
                // do nothing
            }
            else if (tree_fringe->GetGameState()->player_to_move_ == ONE) {
                for (const auto& domino_option :
                    tree_fringe->GetGameState()->player_one_hand_) {
                    if (domino_option->CanConnect(
                            tree_fringe->GetGameState()->
                            domino_train_arms_[i]->GetDominoTrainFringe())) {
                        MctsNode::MoveFactory(tree_fringe, domino_option, i);
                    }
                }
            }
            else {
                for (const auto& domino_option :
                        tree_fringe->GetGameState()->player_two_hand_) {
                    if (domino_option->CanConnect(
                            tree_fringe->GetGameState()->
                                    domino_train_arms_[i]->GetDominoTrainFringe())) {
                        MctsNode::MoveFactory(tree_fringe, domino_option, i);
                    }
                }
            }
            fringes.push_back(tree_fringe->GetGameState()->
                    domino_train_arms_[i]->GetDominoTrainFringe());
        }
        if (tree_fringe->GetNumberChildren() == 0) {
            if (tree_fringe->GetGameState()->pile_.empty()) {
                MctsNode::PassFactory(tree_fringe);
            }
            else {
                for (int i = 0; i < tree_fringe->GetGameState()->pile_.size(); i++) {
                    MctsNode::DrawFactory(tree_fringe, i);
                }
            }
        }
    }

    void Mcts::SimulateFringes(MctsNode *tree_fringe_parent) {
        std::vector<std::thread> threads;
        threads.reserve(tree_fringe_parent->GetNumberChildren());
        for(auto fringe : tree_fringe_parent->GetChildren()) {
            threads.emplace_back(std::thread(SimulateFringe, fringe));
        }
        for (auto& thread : threads) {
            thread.join();
        }
    }

    void Mcts::SimulateFringe(MctsNode* tree_fringe) {
        std::random_device rd;
        std::mt19937 rng(rd());
        dominomcts::MctsNode* working_fringe = tree_fringe;
        while (!IsGameOver(working_fringe)) {
            ExpandFringe(working_fringe);
            std::uniform_int_distribution<int> uni_dist(0,
                    working_fringe->GetNumberChildren()-1);
            working_fringe = working_fringe->GetChildren()[uni_dist(rng)];
        }
        tree_fringe->Update(DetermineWinner(working_fringe));
        if (working_fringe == tree_fringe) {
            working_fringe->DeleteChildren();
        } else {
            do {
                working_fringe = working_fringe->GetParent();
                working_fringe->DeleteChildren();
            } while (working_fringe != tree_fringe);
        }
    }

    void Mcts::BackpropagateExpandedFringe(MctsNode *tree_fringe_parent) {
        int player_one_wins = 0;
        int simulations = tree_fringe_parent->GetNumberChildren();
        for (auto fringe : tree_fringe_parent->GetChildren()) {
            if (fringe->GetGameState()->player_to_move_ == ONE) {
                player_one_wins += fringe->GetPlayerWins();
            } else if(fringe->GetGameState()->player_to_move_ == TWO) {
                player_one_wins +=
                        (fringe->GetSimulations() - fringe->GetPlayerWins());
            }
        }
        auto current_node = tree_fringe_parent->GetChildren()[0];
        do {
            current_node = current_node->GetParent();
            current_node->UpdateWithPlayerOneWins(player_one_wins, simulations);
        } while (!current_node->IsMctsRoot());
    }

    void Mcts::BackpropagateFinalFringe(MctsNode* final_fringe, Player winner) {
        auto current_node = final_fringe;
        do {
            current_node = current_node->GetParent();
            current_node->Update(winner);
        } while (!current_node->IsMctsRoot());
    }

    bool Mcts::IsGameOver(MctsNode *node) {
        if (node->GetGameState()->player_one_hand_.empty()) return true;
        if (node->GetGameState()->player_two_hand_.empty()) return true;
        if (node->GetParent() == nullptr || node->GetParent()->GetParent() == nullptr) return false;
        if (node->GetGameState()->player_one_hand_.size() ==
                node->GetParent()->GetGameState()->player_one_hand_.size() &&
            node->GetParent()->GetParent()->GetGameState()->player_one_hand_.size() ==
                node->GetParent()->GetGameState()->player_one_hand_.size() &&
            node->GetGameState()->player_two_hand_.size() ==
                node->GetParent()->GetGameState()->player_two_hand_.size() &&
            node->GetParent()->GetParent()->GetGameState()->player_two_hand_.size() ==
            node->GetParent()->GetGameState()->player_two_hand_.size())
            return true;
        return false;
    }

    Player Mcts::DetermineWinner(MctsNode *node) {
        if (node->GetGameState()->player_one_hand_.empty()) return ONE;
        if (node->GetGameState()->player_two_hand_.empty()) return TWO;
        int player_one_score = 0;
        for (const auto& domino : node->GetGameState()->player_one_hand_) {
            player_one_score += domino->GetValue();
        }
        int player_two_score = 0;
        for (const auto& domino : node->GetGameState()->player_two_hand_) {
            player_two_score += domino->GetValue();
        }
        return player_one_score < player_two_score ? ONE : TWO;
    }
}